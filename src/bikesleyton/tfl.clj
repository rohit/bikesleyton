(ns bikesleyton.tfl
  (:require
   [clj-http.client :as client]
   [clojure.xml :as xml]
   [hiccup.core :as hiccup]
   [yada.yada :as yada])
  (:import (java.io ByteArrayInputStream)))

(defn parse-xml
  [s]
  (xml/parse
   (ByteArrayInputStream. (.getBytes s))))

(def URL "http://www.tfl.gov.uk/tfl/syndication/feeds/cycle-hire/livecyclehireupdates.xml")

(def LEYTON {:lat 51.5619
             :long -0.013139})

(defn degrees->radians
  [x]
  (Math/toRadians x))

(def radius 6371000)

;; http://www.movable-type.co.uk/scripts/latlong.html
;; Returns distance in meters
(defn- distance
  [a b]
  (let [d-long  (degrees->radians (- (:long b) (:long a)))
        d-lat   (degrees->radians (- (:lat b) (:lat a)))
        a       (+ (* (Math/sin (/ d-lat 2))
                      (Math/sin (/ d-lat 2)))
                   (* (Math/cos (degrees->radians (:lat a)))
                      (Math/cos (degrees->radians (:lat b)))
                      (Math/sin (/ d-long 2))
                      (Math/sin (/ d-long 2))))]
    (* radius 2 (Math/atan2 (Math/sqrt a)
                            (Math/sqrt (- 1 a))))))

(defmulti parse-field (fn [y] (:tag y)))

(defmethod parse-field :lat
  [y]
  (Float/valueOf (first (:content y))))

(defmethod parse-field :long
  [y]
  (Float/valueOf (first (:content y))))

(defmethod parse-field :default
  [y]
  (first (:content y)))

(defn xml->point
  [x]
  (into {}
        (map (fn [y]
               [(:tag y) (parse-field y)])
             (:content x))))

(defn- to->html
  [xs]
  (hiccup/html
   [:html {:lang "en"}
    [:head
     [:meta {:chartset "utf-8"}]
     [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
     [:title "5 Closest Bike Stations to Leyton"]
     [:link {:href "https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
             :rel "stylesheet"}]]
    [:body.container
     [:h2 "5 Closest Bike Stations to Leyton are listed below"]
     [:table.table
      [:thead
       [:tr
        [:th "Name"]
        [:th "Latitude"]
        [:th "Longitude"]
        [:th "Bikes Available"]]]
      [:tbody
       (for [x xs]
         [:tr
          [:td (:name x)]
          [:td (:lat x)]
          [:td (:long x)]
          [:td (:nbBikes x)]])]]]]))

(defn get-bikes
  [ctx]
  (let [feed     (client/get URL)
        stations (->> (parse-xml (:body feed))
                      :content
                      (map xml->point)
                      (sort-by #(distance LEYTON %)))
        closest  (take 5 stations)]
    (if (= "application/json" (yada/content-type ctx))
      closest
      (to->html closest))))

(defn closest-bikes
  []
  ["/closest-bikes"
   (yada/resource
    {:methods
     {:get
      {:produces {:media-type #{"application/json" "text/html"}}
       :response get-bikes}}
     :access-control {:scheme "Basic"
                      :verify (fn [[user password]]
                                (when (= [user password] ["guest" "guest"])
                                  {:user "guest"
                                   :roles #{"sys/user"}}))
                      :authorization {:methods {:get "sys/user"}}}})])
