(ns bikesleyton.web-server
  (:require
   [bidi.vhosts :refer [make-handler vhosts-model]]
   [com.stuartsierra.component :refer [Lifecycle using]]
   [bikesleyton.tfl :refer [closest-bikes]]
   [yada.yada :as yada]))

(defn routes
  [config]
  [""
   [(closest-bikes)]])

(defrecord WebServer [config listener]
  Lifecycle
  (start
    [component]
    (println "Starting webserver...")
    (if listener
      component
      (let [vhosts-model  (vhosts-model [{:scheme :http
                                          :host (:host config)}
                                         (routes config)])
            listener      (yada/listener vhosts-model {:port (:port config)})]
        (println (format "Started web-server on port %s" (:port listener)))
        (assoc component :listener listener))))

  (stop
    [component]
    (when-let [close (get-in component [:listener :close])]
      (close))
    (assoc component :listener nil)))

(defn new-web-server
  [config]
  (using
   (map->WebServer {:config config})
   []))