(ns bikesleyton.system
  (:require
   [bikesleyton.web-server :refer [new-web-server]]
   [com.stuartsierra.component :refer [system-map system-using using]]
   [envoy.core :as env :refer [defenv env]]))

(defenv :port
  "TCP port to run the HTTP server on."
  :type :integer)

(defenv :host
  "Host name"
  :type :string)

(defn new-system-map
  [config]
  (system-map
   :web-server (new-web-server config)))

(defn new-dependency-map
  []
  {})

(def config (select-keys env [:port :host]))

(defn new-production-system
  []
  (-> (new-system-map config)
      (system-using (new-dependency-map))))