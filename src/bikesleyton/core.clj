(ns bikesleyton.core
  (:require [com.stuartsierra.component :as component]
            [bikesleyton.system :refer [new-production-system]])
  (:gen-class))

(def system nil)

(defn -main
  [& args]
  (let [system (new-production-system)]
    (component/start system))
  ;; All threads are daemon, so block forever:
  @(promise))