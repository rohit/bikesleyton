(defproject bikesleyton "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [bidi "2.0.17"]
                 [yada "1.2.2"]
                 [com.stuartsierra/component "0.3.2"]
                 [amperity/envoy "0.3.1"]
                 [clj-http "3.5.0"]
                 [hiccup "1.0.5"]]
  :main ^:skip-aot bikesleyton.core
  :global-vars {*warn-on-reflection* true}
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}
             :repl {:source-paths ["dev"]
                    :repl-options {:init-ns user
                                   :welcome (println "Type (go) to start")}
                    :dependencies [[reloaded.repl "0.2.3"]]
                    :plugins [[lein-environ "1.1.0"]]
                    :env {:host "localhost:8090"
                          :port 8090}}})
