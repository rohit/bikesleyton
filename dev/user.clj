(ns user
  (:require
   [com.stuartsierra.component :as component]
   [bikesleyton.system :as system]
   [reloaded.repl :refer [system init start stop go reset reset-all]]))

(defn new-dev-system
  "Create a development system"
  []
  (let [config system/config]
    (component/system-using
      (system/new-system-map config)
      (system/new-dependency-map))))

(reloaded.repl/set-init! new-dev-system)
