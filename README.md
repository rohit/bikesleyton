# Sample program to return 5 closest bike stations to Leyton

## Usage

In the commandline, launch repl:

```
➜ lein repl

user=> (go)
```

Now in another terminal window, run the following command:

```
curl -u guest:guest -H "Accept: application/json" http://localhost:8090/closest-bikes
```

This returns the result set as a json.

To get the results as HTML in cURL,

```
curl -u guest:guest -H "Accept: text/html" http://localhost:8090/closest-bikes
```

You can alternatively open `http://localhost:8090/closest-bikes` in a browser to see the results as HTML.